﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;

namespace MQTTMessengerServiceConsole
{
    class Program : ServiceBase
    {
        static void Main(string[] args)
        {
            ServiceBase.Run(new Program());
        }

        public Program()
        {
            this.ServiceName = "MQTTMessengerService";
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            //TODO: place your start code here
        }

        protected override void OnStop()
        {
            base.OnStop();

            //TODO: clean up any variables and stop any threads
        }
    }
}
